//------------------------------------------------------------------------------
// <copyright file="NuiImpl.cpp" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

// Implementation of CDepthTrackingApp methods dealing with NUI processing
#include "stdafx.h"

#include "DepthTracking.h"
#include "resource.h"
#include <mmsystem.h>
#include <assert.h>
#include <strsafe.h>

using namespace std;

int TIME = 0;
//lookups for color tinting based on player index
static const int g_IntensityShiftByPlayerR[] = { 1, 2, 0, 2, 0, 0, 2, 0 };
static const int g_IntensityShiftByPlayerG[] = { 1, 2, 2, 0, 2, 0, 0, 1 };
static const int g_IntensityShiftByPlayerB[] = { 1, 0, 2, 2, 0, 2, 0, 2 };


//-------------------------------------------------------------------
// Nui_Zero
//
// Zero out member variables
//-------------------------------------------------------------------
void CDepthTrackingApp::Nui_Zero()
{
    if (m_pNuiSensor)
    {
        m_pNuiSensor->Release();
        m_pNuiSensor = NULL;
    }
    m_hNextDepthFrameEvent = NULL;
    m_hNextColorFrameEvent = NULL;
    m_pDepthStreamHandle = NULL;
    m_pVideoStreamHandle = NULL;
    m_hThNuiProcess = NULL;
    m_hEvNuiProcessStop = NULL;
    m_bScreenBlanked = false;
    m_DepthFramesTotal = 0;
    m_LastDepthFPStime = 0;
    m_LastDepthFramesTotal = 0;
    m_pDrawDepth = NULL;
    m_pDrawColor = NULL;

	for (int i = 0; i < 10; i++) {
		m_depthMax[i] = 0;
		m_depthMin[i] = 4095;
		m_depthMean[i] = 0;
		m_DepthTrend[i] = -1;
		m_LastDepthMax[i] = 0;
		m_LastDepthMin[i] = 4095;
		m_LastDepthMean[i] = 0;
		m_LastDepthTrend[i] = -1;
	}

	for (int i = 0; i < 4; i++) {
		m_DepthTrendOccurrences[i] = -1;
	}

	for (int i = 0; i <= 3; i++) {
		bool_decrease[i] = false;
		bool_zero[i] = false;
		bool_nothing[i] = false;
	}
}

void CALLBACK CDepthTrackingApp::Nui_StatusProcThunk( HRESULT hrStatus, const OLECHAR* instanceName, const OLECHAR* uniqueDeviceName, void * pUserData )
{
    reinterpret_cast<CDepthTrackingApp *>(pUserData)->Nui_StatusProc( hrStatus, instanceName, uniqueDeviceName );
}

//-------------------------------------------------------------------
// Nui_StatusProc
//
// Callback to handle Kinect status changes
//-------------------------------------------------------------------
void CALLBACK CDepthTrackingApp::Nui_StatusProc( HRESULT hrStatus, const OLECHAR* instanceName, const OLECHAR* uniqueDeviceName )
{
    // Update UI
    PostMessageW( m_hWnd, WM_USER_UPDATE_COMBO, 0, 0 );

    if( SUCCEEDED(hrStatus) )
    {
        if ( S_OK == hrStatus )
        {
            if ( m_instanceId && 0 == wcscmp(instanceName, m_instanceId) )
            {
                Nui_Init(m_instanceId);
            }
            else if ( !m_pNuiSensor )
            {
                Nui_Init();
            }
        }
    }
    else
    {
        if ( m_instanceId && 0 == wcscmp(instanceName, m_instanceId) )
        {
            Nui_UnInit();
            Nui_Zero();
        }
    }
}

//-------------------------------------------------------------------
// Nui_Init
//
// Initialize Kinect by instance name
//-------------------------------------------------------------------
HRESULT CDepthTrackingApp::Nui_Init( OLECHAR *instanceName )
{
    // Generic creation failure
    if ( NULL == instanceName )
    {
        MessageBoxResource( IDS_ERROR_NUICREATE, MB_OK | MB_ICONHAND );
        return E_FAIL;
    }

    HRESULT hr = NuiCreateSensorById( instanceName, &m_pNuiSensor );
    
    // Generic creation failure
    if ( FAILED(hr) )
    {
        MessageBoxResource( IDS_ERROR_NUICREATE, MB_OK | MB_ICONHAND );
        return hr;
    }

    SysFreeString(m_instanceId);

    m_instanceId = m_pNuiSensor->NuiDeviceConnectionId();

    return Nui_Init();
}

//-------------------------------------------------------------------
// Nui_Init
//
// Initialize Kinect
//-------------------------------------------------------------------
HRESULT CDepthTrackingApp::Nui_Init( )
{
    HRESULT  hr;
    RECT     rc;
    bool     result;

    if ( !m_pNuiSensor )
    {
        HRESULT hr = NuiCreateSensorByIndex(0, &m_pNuiSensor);

        if ( FAILED(hr) )
        {
            return hr;
        }

        SysFreeString(m_instanceId);

        m_instanceId = m_pNuiSensor->NuiDeviceConnectionId();
    }

    m_hNextDepthFrameEvent = CreateEvent( NULL, TRUE, FALSE, NULL );
    m_hNextColorFrameEvent = CreateEvent( NULL, TRUE, FALSE, NULL );

    m_pDrawDepth = new DrawDevice( );
    result = m_pDrawDepth->Initialize( GetDlgItem( m_hWnd, IDC_DEPTHVIEWER ), m_pD2DFactory, 640, 480, 640 * 4 );
    if ( !result )
    {
        MessageBoxResource( IDS_ERROR_DRAWDEVICE, MB_OK | MB_ICONHAND );
        return E_FAIL;
    }

    m_pDrawColor = new DrawDevice( );
    result = m_pDrawColor->Initialize( GetDlgItem( m_hWnd, IDC_VIDEOVIEW ), m_pD2DFactory, 640, 480, 640 * 4 );
    if ( !result )
    {
        MessageBoxResource( IDS_ERROR_DRAWDEVICE, MB_OK | MB_ICONHAND );
        return E_FAIL;
    }
    
    DWORD nuiFlags = NUI_INITIALIZE_FLAG_USES_DEPTH_AND_PLAYER_INDEX |  NUI_INITIALIZE_FLAG_USES_COLOR;
    hr = m_pNuiSensor->NuiInitialize( nuiFlags );
  
    if ( FAILED( hr ) )
    {
        if ( E_NUI_DEVICE_IN_USE == hr )
        {
            MessageBoxResource( IDS_ERROR_IN_USE, MB_OK | MB_ICONHAND );
        }
        else
        {
            MessageBoxResource( IDS_ERROR_NUIINIT, MB_OK | MB_ICONHAND );
        }
        return hr;
    }
    hr = m_pNuiSensor->NuiImageStreamOpen(
        NUI_IMAGE_TYPE_COLOR,
        NUI_IMAGE_RESOLUTION_640x480,
        0,
        2,
        m_hNextColorFrameEvent,
        &m_pVideoStreamHandle );

    if ( FAILED( hr ) )
    {
        MessageBoxResource( IDS_ERROR_VIDEOSTREAM, MB_OK | MB_ICONHAND );
        return hr;
    }

    hr = m_pNuiSensor->NuiImageStreamOpen(
        HasSkeletalEngine(m_pNuiSensor) ? NUI_IMAGE_TYPE_DEPTH_AND_PLAYER_INDEX : NUI_IMAGE_TYPE_DEPTH,
		NUI_IMAGE_RESOLUTION_640x480,
        1,
        2,
        m_hNextDepthFrameEvent,
        &m_pDepthStreamHandle );

    if ( FAILED( hr ) )
    {
        MessageBoxResource(IDS_ERROR_DEPTHSTREAM, MB_OK | MB_ICONHAND);
        return hr;
    }


    // Start the Nui processing thread
    m_hEvNuiProcessStop = CreateEvent( NULL, FALSE, FALSE, NULL );
    m_hThNuiProcess = CreateThread( NULL, 0, Nui_ProcessThread, this, 0, NULL );

    return hr;
}

//-------------------------------------------------------------------
// Nui_UnInit
//
// Uninitialize Kinect
//-------------------------------------------------------------------
void CDepthTrackingApp::Nui_UnInit( )
{
    // Stop the Nui processing thread
    if ( NULL != m_hEvNuiProcessStop )
    {
        // Signal the thread
        SetEvent(m_hEvNuiProcessStop);

        // Wait for thread to stop
        if ( NULL != m_hThNuiProcess )
        {
            WaitForSingleObject( m_hThNuiProcess, INFINITE );
            CloseHandle( m_hThNuiProcess );
        }
        CloseHandle( m_hEvNuiProcessStop );
    }

    if ( m_pNuiSensor )
    {
        m_pNuiSensor->NuiShutdown( );
    }
    if ( m_hNextDepthFrameEvent && ( m_hNextDepthFrameEvent != INVALID_HANDLE_VALUE ) )
    {
        CloseHandle( m_hNextDepthFrameEvent );
        m_hNextDepthFrameEvent = NULL;
    }
    if ( m_hNextColorFrameEvent && ( m_hNextColorFrameEvent != INVALID_HANDLE_VALUE ) )
    {
        CloseHandle( m_hNextColorFrameEvent );
        m_hNextColorFrameEvent = NULL;
    }

    if ( m_pNuiSensor )
    {
        m_pNuiSensor->Release();
        m_pNuiSensor = NULL;
    }

    // clean up graphics
    delete m_pDrawDepth;
    m_pDrawDepth = NULL;

    delete m_pDrawColor;
    m_pDrawColor = NULL;    
}

DWORD WINAPI CDepthTrackingApp::Nui_ProcessThread(LPVOID pParam)
{
    CDepthTrackingApp *pthis = (CDepthTrackingApp *) pParam;
    return pthis->Nui_ProcessThread();
}

//-------------------------------------------------------------------
// Nui_ProcessThread
//
// Thread to handle Kinect processing
//-------------------------------------------------------------------
DWORD WINAPI CDepthTrackingApp::Nui_ProcessThread()
{
    const int numEvents = 3;
	HANDLE hEvents[numEvents] = { m_hEvNuiProcessStop, m_hNextDepthFrameEvent, m_hNextColorFrameEvent };
    int    nEventIdx;
    DWORD  t;

    m_LastDepthFPStime = timeGetTime( );

    // Main thread loop
    bool continueProcessing = true;
    while ( continueProcessing )
    {
        // Wait for any of the events to be signalled
        nEventIdx = WaitForMultipleObjects( numEvents, hEvents, FALSE, 100 );
		
		TIME++;

        // Process signal events
        switch ( nEventIdx )
        {
            case WAIT_TIMEOUT:
                continue;

            // If the stop event, stop looping and exit
            case WAIT_OBJECT_0:
                continueProcessing = false;
                continue;

            case WAIT_OBJECT_0 + 1:
                Nui_GotDepthAlert();
                 ++m_DepthFramesTotal;

				 if (g_DepthModuleOn == true) {
					 int alert = 0;
					 SoundAlert_DepthTrend_Trigger(alert);
					 //Sleep(0.05 * 1000);
					 LPCTSTR depth = _T(" ");
					 switch (alert) {
					 case 1:
						 depth = _T("LEFT NEARER");
						 break;
					 case 2:
						 depth = _T("CENTER NEARER");
						 break;
					 case 3:
						 depth = _T("RIGHT NEARER");
						 break;
					 case 4:
						 depth = _T("LEFT OUT OF RANGE");
						 break;
					 case 5:
						 depth = _T("CENTER OUT OF RANGE");
						 break;
					 case 6:
						 depth = _T("RIGHT OUT OF RANGE");
						 break;
					 }
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH, IDC_DEPTH, (LPARAM)((LPCTSTR)depth));
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPLEFT_MIN, IDC_DEPTH_TOPLEFT_MIN, m_depthMin[1]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPLEFT_MAX, IDC_DEPTH_TOPLEFT_MAX, m_depthMax[1]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPLEFT_MEAN, IDC_DEPTH_TOPLEFT_MEAN, m_depthMean[1]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPCENTER_MIN, IDC_DEPTH_TOPCENTER_MIN, m_depthMin[2]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPCENTER_MAX, IDC_DEPTH_TOPCENTER_MAX, m_depthMax[2]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPCENTER_MEAN, IDC_DEPTH_TOPCENTER_MEAN, m_depthMean[2]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPRIGHT_MIN, IDC_DEPTH_TOPRIGHT_MIN, m_depthMin[3]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPRIGHT_MAX, IDC_DEPTH_TOPRIGHT_MAX, m_depthMax[3]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPRIGHT_MEAN, IDC_DEPTH_TOPRIGHT_MEAN, m_depthMean[3]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERLEFT_MIN, IDC_DEPTH_CENTERLEFT_MIN, m_depthMin[4]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERLEFT_MAX, IDC_DEPTH_CENTERLEFT_MAX, m_depthMax[4]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERLEFT_MEAN, IDC_DEPTH_CENTERLEFT_MEAN, m_depthMean[4]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERCENTER_MIN, IDC_DEPTH_CENTERCENTER_MIN, m_depthMin[5]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERCENTER_MAX, IDC_DEPTH_CENTERCENTER_MAX, m_depthMax[5]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERCENTER_MEAN, IDC_DEPTH_CENTERCENTER_MEAN, m_depthMean[5]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERRIGHT_MIN, IDC_DEPTH_CENTERRIGHT_MIN, m_depthMin[6]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERRIGHT_MAX, IDC_DEPTH_CENTERRIGHT_MAX, m_depthMax[6]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERRIGHT_MEAN, IDC_DEPTH_CENTERRIGHT_MEAN, m_depthMean[6]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMLEFT_MIN, IDC_DEPTH_BOTTOMLEFT_MIN, m_depthMin[7]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMLEFT_MAX, IDC_DEPTH_BOTTOMLEFT_MAX, m_depthMax[7]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMLEFT_MEAN, IDC_DEPTH_BOTTOMLEFT_MEAN, m_depthMean[7]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMCENTER_MIN, IDC_DEPTH_BOTTOMCENTER_MIN, m_depthMin[8]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMCENTER_MAX, IDC_DEPTH_BOTTOMCENTER_MAX, m_depthMax[8]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMCENTER_MEAN, IDC_DEPTH_BOTTOMCENTER_MEAN, m_depthMean[8]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMRIGHT_MIN, IDC_DEPTH_BOTTOMRIGHT_MIN, m_depthMin[9]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMRIGHT_MAX, IDC_DEPTH_BOTTOMRIGHT_MAX, m_depthMax[9]);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMRIGHT_MEAN, IDC_DEPTH_BOTTOMRIGHT_MEAN, m_depthMean[9]);
				 }
				 else {
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH, IDC_DEPTH, (LPARAM)((LPCTSTR)_T("--")));
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPLEFT_MIN, IDC_DEPTH_TOPLEFT_MIN, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPLEFT_MAX, IDC_DEPTH_TOPLEFT_MAX, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPLEFT_MEAN, IDC_DEPTH_TOPLEFT_MEAN, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPCENTER_MIN, IDC_DEPTH_TOPCENTER_MIN, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPCENTER_MAX, IDC_DEPTH_TOPCENTER_MAX, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPCENTER_MEAN, IDC_DEPTH_TOPCENTER_MEAN, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPRIGHT_MIN, IDC_DEPTH_TOPRIGHT_MIN, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPRIGHT_MAX, IDC_DEPTH_TOPRIGHT_MAX, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_TOPRIGHT_MEAN, IDC_DEPTH_TOPRIGHT_MEAN, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERLEFT_MIN, IDC_DEPTH_CENTERLEFT_MIN, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERLEFT_MAX, IDC_DEPTH_CENTERLEFT_MAX, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERLEFT_MEAN, IDC_DEPTH_CENTERLEFT_MEAN, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERCENTER_MIN, IDC_DEPTH_CENTERCENTER_MIN, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERCENTER_MAX, IDC_DEPTH_CENTERCENTER_MAX, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERCENTER_MEAN, IDC_DEPTH_CENTERCENTER_MEAN, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERRIGHT_MIN, IDC_DEPTH_CENTERRIGHT_MIN, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERRIGHT_MAX, IDC_DEPTH_CENTERRIGHT_MAX, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_CENTERRIGHT_MEAN, IDC_DEPTH_CENTERRIGHT_MEAN, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMLEFT_MIN, IDC_DEPTH_BOTTOMLEFT_MIN, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMLEFT_MAX, IDC_DEPTH_BOTTOMLEFT_MAX, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMLEFT_MEAN, IDC_DEPTH_BOTTOMLEFT_MEAN, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMCENTER_MIN, IDC_DEPTH_BOTTOMCENTER_MIN, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMCENTER_MAX, IDC_DEPTH_BOTTOMCENTER_MAX, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMCENTER_MEAN, IDC_DEPTH_BOTTOMCENTER_MEAN, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMRIGHT_MIN, IDC_DEPTH_BOTTOMRIGHT_MIN, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMRIGHT_MAX, IDC_DEPTH_BOTTOMRIGHT_MAX, 0);
					 PostMessageW(m_hWnd, WM_USER_UPDATE_DEPTH_BOTTOMRIGHT_MEAN, IDC_DEPTH_BOTTOMRIGHT_MEAN, 0);
				 }
                break;

            case WAIT_OBJECT_0 + 2:
                Nui_GotColorAlert();
                break;
        }

        // Once per second, display the depth FPS
        t = timeGetTime( );
        if ( (t - m_LastDepthFPStime) > 1000 )
        {
            int fps = ((m_DepthFramesTotal - m_LastDepthFramesTotal) * 1000 + 500) / (t - m_LastDepthFPStime);
            PostMessageW( m_hWnd, WM_USER_UPDATE_FPS, IDC_FPS, fps );
            m_LastDepthFramesTotal = m_DepthFramesTotal;
            m_LastDepthFPStime = t;
        }

    }

    return 0;
}

//-------------------------------------------------------------------
// Nui_GotDepthAlert
//
// Handle new color data
//-------------------------------------------------------------------
void CDepthTrackingApp::Nui_GotColorAlert( )
{

	/*ofstream outFile;
	outFile.open("DEBUG.txt", ios::out | ios::app);
	if (outFile.is_open() )
		outFile << "T " << TIME << "\n";
		
	outFile.close();*/

    NUI_IMAGE_FRAME imageFrame;

    HRESULT hr = m_pNuiSensor->NuiImageStreamGetNextFrame( m_pVideoStreamHandle, 0, &imageFrame );

    if ( FAILED( hr ) )
    {
        return;
    }

    INuiFrameTexture * pTexture = imageFrame.pFrameTexture;
    NUI_LOCKED_RECT LockedRect;
    pTexture->LockRect( 0, &LockedRect, NULL, 0 );
    if ( LockedRect.Pitch != 0 )
    {
        m_pDrawColor->Draw( static_cast<BYTE *>(LockedRect.pBits), LockedRect.size );
    }
    else
    {
        OutputDebugString( L"Buffer length of received texture is bogus\r\n" );
    }

    pTexture->UnlockRect( 0 );

    m_pNuiSensor->NuiImageStreamReleaseFrame( m_pVideoStreamHandle, &imageFrame );

}

//-------------------------------------------------------------------
// Nui_GotDepthAlert
//
// Handle new depth data
//-------------------------------------------------------------------
void CDepthTrackingApp::Nui_GotDepthAlert( )
{
	/*ofstream outFile;
	outFile.open("DEBUG.txt", ios::out | ios::app);
	if (outFile.is_open() )
		outFile << "T " << TIME << "\n";
		
	outFile.close();*/

    NUI_IMAGE_FRAME imageFrame;

    HRESULT hr = m_pNuiSensor->NuiImageStreamGetNextFrame(
        m_pDepthStreamHandle,
        0,
        &imageFrame );

    if ( FAILED( hr ) )
    {
        return;
    }

    INuiFrameTexture * pTexture = imageFrame.pFrameTexture;
    NUI_LOCKED_RECT LockedRect;
    pTexture->LockRect( 0, &LockedRect, NULL, 0 );
    if ( 0 != LockedRect.Pitch )
    {
        DWORD frameWidth, frameHeight;
        
        NuiImageResolutionToSize( imageFrame.eResolution, frameWidth, frameHeight );
        
		vector<USHORT> realDepthRun = m_realDepth;

        // draw the bits to the bitmap
        RGBQUAD * rgbrun = m_rgbWk;
        USHORT * pBufferRun = (USHORT *)LockedRect.pBits;

        // end pixel is start + width*height - 1
        USHORT * pBufferEnd = pBufferRun + (frameWidth * frameHeight);

        assert( frameWidth * frameHeight <= ARRAYSIZE(m_rgbWk) );

        while ( pBufferRun < pBufferEnd )
		{
			realDepthRun.push_back(NuiDepthPixelToDepth(*pBufferRun));
            *rgbrun = Nui_ShortToQuad_Depth( *pBufferRun );
            ++pBufferRun;
            ++rgbrun;
		}

		if (g_DepthModuleOn == true) {
			Vector_SplitToNine(realDepthRun, frameWidth, frameHeight, m_depthSection[1], m_depthSection[2], m_depthSection[3], m_depthSection[4], m_depthSection[5], m_depthSection[6], m_depthSection[7], m_depthSection[8], m_depthSection[9]);

			m_depthSection[0] = realDepthRun;

			for (int i = 0; i < 10; i++) {
				Vector_MinMax(m_depthSection[i], m_depthMin[i], m_depthMax[i]);
				Vector_Mean(m_depthSection[i], m_depthMean[i]);
			}

			#ifdef _RECORDS
				DepthData_To_TextFile("DEPTH_TRACKING_LAST.txt", "last");
				DepthData_To_TextFile("DEPTH_TRACKING.txt", "current");
			#endif

			DepthData_Get_Trend();

			// Save current depth data as last for next time
			for (int i = 0; i < 10; i++) {
				m_LastDepthMax[i] = m_depthMax[i];
				m_LastDepthMin[i] = m_depthMin[i];
				m_LastDepthMean[i] = m_depthMean[i];
				m_LastDepthTrend[i] = m_DepthTrend[i];
			}
		}

        m_pDrawDepth->Draw( (BYTE*) m_rgbWk, frameWidth * frameHeight * 4 );
    }
    else
    {
        OutputDebugString( L"Buffer length of received texture is bogus\r\n" );
    }

    pTexture->UnlockRect( 0 );

    m_pNuiSensor->NuiImageStreamReleaseFrame( m_pDepthStreamHandle, &imageFrame );
}

// Movement Trend Track
void CDepthTrackingApp::DepthData_Get_Trend()
{
	// Compare last and current depth value
	for (int i = 0; i < 10; i++) {
		// m_DepthTrend: -1 = error, 0 = equal (same), 1 = increasing (far), 2 = decreasing (near), 3 = significant decreasing, 4 = decreasing (near) in certain range, 5 = zero, 6 = significant decreasing in certain range
		int deviation = 20;
		int certain_min_range = 1200;
		int significant_change = 400;
		m_DepthTrend[i] = -1;
		// if-statements to detect the trend
		if (m_depthMax[i] == 0 && m_LastDepthMax[i] == 0) {
			m_DepthTrend[i] = 5;
		}
		else if (
			m_LastDepthMin[i] - m_depthMin[i] > significant_change
			) {
			if (m_depthMin[i] < certain_min_range)
			{
				m_DepthTrend[i] = 6;
			}
			else
			{
				m_DepthTrend[i] = 3;
			}
		}
		else if(
			m_depthMean[i] >= m_LastDepthMean[i] - deviation && m_depthMean[i] <= m_LastDepthMean[i] + deviation &&
			m_depthMin[i] >= m_LastDepthMin[i] - deviation && m_depthMin[i] <= m_LastDepthMin[i] + deviation &&
			m_depthMax[i] >= m_LastDepthMax[i] - deviation && m_depthMax[i] <= m_LastDepthMax[i] + deviation
			) {
			m_DepthTrend[i] = 0;
		}
		else if (
			m_depthMin[i] < m_LastDepthMin[i] - deviation
			) {
			if (m_depthMin[i] < certain_min_range)
			{
				m_DepthTrend[i] = 4;
			}
			else
			{
				m_DepthTrend[i] = 2;
			}
		}
		else if (
			m_depthMin[i] > m_LastDepthMin[i] - deviation
			) {
			m_DepthTrend[i] = 1;
		}
		/*if (m_depthMean[i] == 0 && m_depthMax[i] == 0 && m_depthMin[i] == 0) {
			m_DepthTrend[i] = 5;
		}
		else if (
			m_LastDepthMin[i] - m_depthMin[i] > significant_change
			) {
			m_DepthTrend[i] = 6;
		}
		else if (
			m_depthMean[i] >= m_LastDepthMean[i] - deviation && m_depthMean[i] <= m_LastDepthMean[i] + deviation &&
			m_depthMin[i] >= m_LastDepthMin[i] - deviation && m_depthMin[i] <= m_LastDepthMin[i] + deviation &&
			m_depthMax[i] >= m_LastDepthMax[i] - deviation && m_depthMax[i] <= m_LastDepthMax[i] + deviation
			) {
			m_DepthTrend[i] = 0;
		} 
		else if (
			m_depthMean[i] > m_LastDepthMean[i] - deviation && 
			m_depthMin[i] > m_LastDepthMin[i] - deviation
			) {
			m_DepthTrend[i] = 1;
		}
		else if (
			m_depthMin[i] < m_LastDepthMin[i] - deviation
			) {
			m_DepthTrend[i] = 2;
		} */
	}

	for (int i = 0; i <= 6; i++) {
		int left_array[3] = { m_DepthTrend[1], m_DepthTrend[4], m_DepthTrend[7] };
		int center_array[3] = { m_DepthTrend[2], m_DepthTrend[5], m_DepthTrend[8] };
		int right_array[3] = { m_DepthTrend[3], m_DepthTrend[6], m_DepthTrend[9] };
		if (std::count(m_DepthTrend + 1, m_DepthTrend + 10, i) >= std::count(m_DepthTrend + 1, m_DepthTrend + 10, m_DepthTrendOccurrences[0])) {
			m_DepthTrendOccurrences[0] = i;
		}
		if (std::count(left_array, left_array + 3, i) >= std::count(left_array, left_array + 3, m_DepthTrendOccurrences[1])) {
			m_DepthTrendOccurrences[1] = i;
		}
		if (std::count(center_array, center_array + 3, i) >= std::count(center_array, center_array + 3, m_DepthTrendOccurrences[2])) {
			m_DepthTrendOccurrences[2] = i;
		}
		if (std::count(right_array, right_array + 3, i) >= std::count(right_array, right_array + 3, m_DepthTrendOccurrences[3])) {
			m_DepthTrendOccurrences[3] = i;
		}
	}

	#ifdef _RECORDS
		DepthData_To_TextFile("DEPTH_TRACKING_TREND.txt", "trend");
	#endif
}

// Sound alert trigger, depends on depth trend
void CDepthTrackingApp::SoundAlert_DepthTrend_Trigger(int &return_value)
{
	if (m_DepthTrendOccurrences[0] == -1 && m_DepthTrendOccurrences[1] == -1 && m_DepthTrendOccurrences[2] == -1 && m_DepthTrendOccurrences[3] == -1) return;

	for (int i = 0; i <= 3; i++) {
		if (m_DepthTrendOccurrences[i] == 4 || m_DepthTrendOccurrences[i] == 6)
		{
			bool_decrease[i] = true;
		}
		else
		{
			bool_decrease[i] = false;
		}
		if (m_DepthTrendOccurrences[i] == 5)
		{
			bool_zero[i] = true;
		}
		else
		{
			bool_zero[i] = false;
		}
		if (bool_decrease[i] || bool_zero[i])
		{
			bool_nothing[i] = false;
		}
		else
		{
			bool_nothing[i] = true;
		}
	}
	
	stringstream ss;

	for (int i = 0; i <= 3; i++) {
		ss << "\t";
		ss << m_DepthTrendOccurrences[i];
		ss << "\t";
	}
	ss << "\n";
	ss << "DECREASE\t";
	for (int i = 0; i <= 3; i++) {
		ss << bool_decrease[i];
		ss << "\t";
	}
	ss << "\n";
	ss << "ZERO\t";
	for (int i = 0; i <= 3; i++) {
		ss << bool_zero[i];
		ss << "\t";
	}
	ss << "\n";
	ss << "NTH\t";
	for (int i = 0; i <= 3; i++) {
		ss << bool_nothing[i];
		ss << "\t";
	}
	ss << "\n";

	if (
		(bool_decrease[1] && bool_decrease[2] && bool_nothing[3]) || //DDX
		(bool_decrease[1] && bool_nothing[2] && bool_nothing[3]) //DXX
		)
	{
		// LEFT NEARER
		return_value = 1;
		PlaySound(TEXT("beep-low-left.wav"), NULL, SND_FILENAME);
		ss << "LEFT NEARER";
	}
	else if (
		(bool_decrease[1] && bool_decrease[2] && bool_decrease[3]) || //DDD
		(bool_decrease[1] && bool_nothing[2] && bool_decrease[3]) || //DXD
		(bool_nothing[1] && bool_decrease[2] && bool_nothing[3]) //XDX
		)
	{
		// CENTER NEARER
		return_value = 2;
		PlaySound(TEXT("beep-low-center.wav"), NULL, SND_FILENAME);
		ss << "CENTER NEARER";
	}
	else if (
		(bool_nothing[1] && bool_decrease[2] && bool_decrease[3]) || //XDD
		(bool_nothing[1] && bool_nothing[2] && bool_decrease[3]) //XXD
		)
	{
		// RIGHT NEARER
		return_value = 3;
		PlaySound(TEXT("beep-low-right.wav"), NULL, SND_FILENAME);
		ss << "RIGHT NEARER";
	}
	else if (
		(bool_zero[1] && bool_decrease[2] && bool_decrease[3]) || //ZDD
		(bool_zero[1] && bool_decrease[2] && bool_nothing[3]) || //ZDX
		(bool_zero[1] && bool_zero[2] && bool_decrease[3]) || //ZZD
		(bool_zero[1] && bool_zero[2] && bool_nothing[3]) || //ZZX
		(bool_zero[1] && bool_nothing[2] && bool_decrease[3]) || //ZXD
		(bool_zero[1] && bool_nothing[2] && bool_nothing[3]) //ZXX
		)
	{
		// LEFT OUT OF RANGE
		return_value = 4;
		PlaySound(TEXT("beep-high-left.wav"), NULL, SND_FILENAME);
		ss << "LEFT OUT OF RANGE";
	}
	else if (
		(bool_decrease[1] && bool_zero[2] && bool_decrease[3]) || //DZD
		(bool_decrease[1] && bool_zero[2] && bool_nothing[3]) || //DZX
		(bool_zero[1] && bool_decrease[2] && bool_zero[3]) || //ZDZ
		(bool_zero[1] && bool_zero[2] && bool_zero[3]) || //ZZZ
		(bool_zero[1] && bool_nothing[2] && bool_zero[3]) || //ZXZ
		(bool_nothing[1] && bool_decrease[2] && bool_zero[3]) || //XDZ
		(bool_nothing[1] && bool_zero[2] && bool_nothing[3]) //XZX
		)
	{
		// CENTER OUT OF RANGE
		return_value = 5;
		PlaySound(TEXT("beep-high-center.wav"), NULL, SND_FILENAME);
		ss << "CENTER OUT OF RANGE";
	}
	else if (
		(bool_decrease[1] && bool_decrease[2] && bool_zero[3]) || //DDZ
		(bool_decrease[1] && bool_zero[2] && bool_zero[3]) || //DZZ
		(bool_decrease[1] && bool_nothing[2] && bool_zero[3]) || //DXZ
		(bool_nothing[1] && bool_decrease[2] && bool_zero[3]) || //XDZ
		(bool_nothing[1] && bool_zero[2] && bool_zero[3]) || //XZZ
		(bool_nothing[1] && bool_nothing[2] && bool_zero[3]) //XXZ
		)
	{
		// RIGHT OUT OF RANGE
		return_value = 6;
		PlaySound(TEXT("beep-high-right.wav"), NULL, SND_FILENAME);
		ss << "RIGHT OUT OF RANGE";
	}
	else
	{
		ss << "NOTHING";
	}
	/*
	ss << "\n";
	ofstream outFile;
	outFile.open("DEBUG.txt", ios::out | ios::app);
	if (outFile.is_open()) {
		outFile << ss.str() << "\n";
	}
	outFile.close();
	*/
}

// Save processed depth data in file
void CDepthTrackingApp::DepthData_To_TextFile(string filename, string mode)
{
	stringstream ss;
	int j = 3;
	for (int i = 0; i < 10; i++) {
		if (i > 0) {
			if (j++ == 3) {
				ss << "\n";
				j = 1;
			}
			else {
				ss << "\t";
			}
			ss << "(" << i << ")" << "\t";
		}
		if (mode == "last") {
			ss << "MIN " << m_LastDepthMin[i] << "mm" << "\t" << "MAX " << m_LastDepthMax[i] << "mm";
			ss << "\t" << "MEAN " << m_LastDepthMean[i] << "mm";
		}
		else if (mode == "current") {
			ss << "MIN " << m_depthMin[i] << "mm" << "\t" << "MAX " << m_depthMax[i] << "mm";
			ss << "\t" << "MEAN " << m_depthMean[i] << "mm";
		}
		else if (mode == "trend") {
			switch (m_DepthTrend[i]) {
			case 0:
				ss << "STAY";
				break;
			case 1:
				ss << "FARTHER";
				break;
			case 2:
				ss << "NEARER";
				break;
			case 3:
				ss << "FAST_NEAR";
				break;
			case 4:
				ss << "NEARER#";
				break;
			case 5:
				ss << "OUT_OF_RANGE";
				break;
			case 6:
				ss << "FAST_NEAR#";
				break;
			case -1:
			default:
				ss << "ERROR";
				break;
			}
		}
	}
	if (mode == "trend") {
		ss << "\n";
		for (int i = 0; i <= 3; i++) {
			switch (m_DepthTrendOccurrences[i]) {
			case 0:
				ss << "STAY";
				break;
			case 1:
				ss << "FARTHER";
				break;
			case 2:
				ss << "NEARER";
				break;
			case 3:
				ss << "FAST_NEAR";
				break;
			case 4:
				ss << "NEARER#";
				break;
			case 5:
				ss << "OUT_OF_RANGE";
				break;
			case 6:
				ss << "FAST_NEAR#";
				break;
			case -1:
			default:
				ss << "ERROR";
				break;
			}
			ss << "\t";
		}
	}

	ss << "\n";
	ofstream outFile;
	outFile.open("Debug\/" + filename, ios::out | ios::app);
	if (outFile.is_open()) {
		outFile << ss.str() << "\n";
	}
	outFile.close();
}

// Split a vector into nine equally
void CDepthTrackingApp::Vector_SplitToNine(vector<USHORT> vect, DWORD frameWidth, DWORD frameHeight,
											vector<USHORT> &vect1,
											vector<USHORT> &vect2, 
											vector<USHORT> &vect3, 
											vector<USHORT> &vect4, 
											vector<USHORT> &vect5, 
											vector<USHORT> &vect6, 
											vector<USHORT> &vect7, 
											vector<USHORT> &vect8, 
											vector<USHORT> &vect9)
{
	vect1.clear();
	vect2.clear();
	vect3.clear();
	vect4.clear();
	vect5.clear();
	vect6.clear();
	vect7.clear();
	vect8.clear();
	vect9.clear();
	int width = 0;
	int height = 0;
	int fullsize = 0;
	int splitWidth = 0;
	int splitHeight = 0;
	fullsize = vect.size();
	splitWidth = (int)(frameWidth / 3) + 1;
	splitHeight = (int)(frameHeight / 3);
	for (int i = 0; i < fullsize; i++) {
		if (width >= frameWidth) {
			width = 0;
			height++;
		}

		if (width >= 0 && width < splitWidth) {
			if (height >= 0 && height < splitHeight) {
				vect1.push_back(vect[i]);
			}
			else if (height >= splitHeight && height < splitHeight * 2) {
				vect4.push_back(vect[i]);
			}
			else if (height >= splitHeight * 2 && height < splitHeight * 3) {
				vect7.push_back(vect[i]);
			}
		}
		else if (width >= splitWidth && width < splitWidth * 2) {
			if (height >= 0 && height < splitHeight) {
				vect2.push_back(vect[i]);
			}
			else if (height >= splitHeight && height < splitHeight * 2) {
				vect5.push_back(vect[i]);
			}
			else if (height >= splitHeight * 2 && height < splitHeight * 3) {
				vect8.push_back(vect[i]);
			}
		}
		else if (width >= splitWidth * 2 && width < splitWidth * 3) {
			if (height >= 0 && height < splitHeight) {
				vect3.push_back(vect[i]);
			}
			else if (height >= splitHeight && height < splitHeight * 2) {
				vect6.push_back(vect[i]);
			}
			else if (height >= splitHeight * 2 && height < splitHeight * 3) {
				vect9.push_back(vect[i]);
			}
		}

		width++;
	}
}

// Calculate Maximum and Minimum Value in a vector
void CDepthTrackingApp::Vector_MinMax(vector<USHORT> vect, USHORT &minValue, USHORT &maxValue)
{
	maxValue = 0;
	minValue = 4095;
	for (std::vector<USHORT>::size_type i = 0; i != vect.size(); i++) {
		// Record the maximum value
		if (vect[i] > maxValue) {
			maxValue = (USHORT)vect[i];
		}
		// Record the minimum value
		if (vect[i] < minValue && vect[i] > 0){
			minValue = (USHORT)vect[i];
		}
		if (vect[i] > 4095) {
			OutputDebugString(L"Depth values contain non-sense value that over 4095\r\n");
		}
	}
	// Assume min and max value stay 4095 and 0, means cannot get depth value as it is out of range
	if (minValue == 4095 && maxValue == 0) {
		minValue = 0;
		maxValue = 0;
	}
}

// Calculate Mean Value in a vector
void CDepthTrackingApp::Vector_Mean(vector<USHORT> vect, USHORT &meanValue)
{
	meanValue = 0;
	if (vect.size() > 0) {
		UINT sum = 0;
		UINT zero = 0;
		// Add each values in the vector
		for (std::vector<USHORT>::iterator it = vect.begin(); it != vect.end(); ++it) {
			sum += *it;
			if (*it == 0) {
				zero++;
			}
		}
		// get base that not zeros
		UINT size_of = 0;
		size_of = vect.size();
		if (size_of - zero > 1) {
			size_of -= zero;
		}
		// get the average
		meanValue = sum / size_of;
	}
}

//---------------------------------------
// Nui_ShortToQuad_Depth
//
// Get the player colored depth value
//-------------------------------------------------------------------
RGBQUAD CDepthTrackingApp::Nui_ShortToQuad_Depth( USHORT s )
{
    USHORT RealDepth = NuiDepthPixelToDepth(s);
    USHORT Player    = NuiDepthPixelToPlayerIndex(s);

    // transform 13-bit depth information into an 8-bit intensity appropriate
    // for display (we disregard information in most significant bit)
    BYTE intensity = (BYTE)~(RealDepth >> 4);

    // tint the intensity by dividing by per-player values
    RGBQUAD color;
    color.rgbRed   = intensity >> g_IntensityShiftByPlayerR[Player];
    color.rgbGreen = intensity >> g_IntensityShiftByPlayerG[Player];
    color.rgbBlue  = intensity >> g_IntensityShiftByPlayerB[Player];

    return color;
}